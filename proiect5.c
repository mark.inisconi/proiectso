#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <libgen.h>

#define PATH_MAX 4096


int Drepturi(struct stat st){//Functie care verifica existenta drepturilor unui fisier
    if((st.st_mode & S_IRUSR) || (st.st_mode & S_IWUSR) || (st.st_mode & S_IXUSR)){
        return 1;
    } else {
        return 0;
    }
}


void Coruptie(char* cale, const char* caleCorupt){//Functie care muta fisierul corupt in folderul specific acestor fisiere
    char destPath[PATH_MAX];
    sprintf(destPath, "%s/%s", caleCorupt, basename(cale));

    if(rename(cale, destPath) != 0){
        perror("Nu s a mutat fisierul\n");
        exit(-1);
    } else {
        printf("S a mutat fisierul corupt\n");
    }
}


//Functia care parseaza  directorul
//CD numele directorului, bin variabila de selectie
void Parse(const char* CD, int bin, int fileDescriptor, const char* Script, const char* caleCorupt){
    int count = 0;
    DIR* dirRef = NULL;
    printf("%s\n", CD);
    if((dirRef = opendir(CD)) == NULL) {//Deschiderea directorului
        perror("Nu s a putut deschide directorul curent\n");
        exit(-1);
    }




    struct dirent* curr = NULL;//intrarea curenta din director
    struct stat st;//info curente din intrarea curenta


    struct stat aux;
    char nume[256];//d_name e tot de 256
    int ok = 0;

    //printf("%s\n", CD);

    char cale[257];//Auxiliar pentru calea intrarii curente

    if(bin == 0){//Daca nu s a creat un snapshot, urmeaza codul:
        while((curr = readdir(dirRef)) != NULL){//Citesc fiecare intrare din director

        if((strcmp(curr->d_name, ".") == 0) || (strcmp(curr->d_name, "..") == 0)){//Ignor directorul curent si cel parinte
            continue;
        }

        sprintf(cale, "%s/%s", CD, curr->d_name);
        if(lstat(cale, &st) == -1){//Obtinerea statisticilor fisierului
            perror("Eroare statistici 1\n");
            exit(-1);
        }

        if(S_ISREG(st.st_mode)){//Verificare daca este fisier
            if(!Drepturi(st)){//Verificare daca nu are drepturi
                count++;//Incrementarea numarului de fisiere corupte din folderul curent
                pid_t pidNepot;
                pid_t pidWaitNepot;
                int status;
                pid_t pfd[2];

                if(pipe(pfd) < 0){//Creearea legaturii dintre procesul parinte si procesul fiu/nepot
                    perror("Eroare pipe file descriptors\n");
                    exit(-1);
                }

                if((pidNepot = fork()) < 0){//Fork creeaza un proces nepot pentru script
                    perror("Eroare\n");
                    exit(-1);
                }

                if(pidNepot == 0){//Codul nepotului
                    if(close(pfd[0]) != 0){//Inchidere citire nepot
                        perror("err pfd\n");
                        exit(-1);
                    }

                    if(dup2(pfd[1],1) == -1){//Redirecteaza iesirea standard catre capatul de scriere
                        perror("err pfd\n");
                        exit(-1);
                    }

                    execl("/bin/bash", "bin", Script, cale, NULL);//Executia scriptului
                    perror("Eroare naspa\n");
                    exit(-1);

                }  else {
                  //cod fiu

                    if(close(pfd[1]) != 0){//Inchid capatul de scriere
                        perror("err pfd\n");
                        exit(-1);
                    }

                    char verdict[256]={0};//variabila utilizata pentru a stii daca fiserul e corupt sau nu

                    if(read(pfd[0], verdict, 256) == -1){//citire din pipe
                        perror("err pfd\n");
                        exit(-1);
                    }

                    verdict[strlen(verdict) - 1] = '\0';//Adaugarea caracterului NULL

                    if(close(pfd[0]) != 0){//Inchiderea pipe ului implicit a capatului de citire
                        perror("err pfd\n");
                        exit(-1);
                    }

                    pidWaitNepot = wait(&status);//Asteptarea finalizarii procesului nepot
                    if(WIFEXITED(status)){
                        printf("Nepot proces %d cu pid-ul %d s a terminat cu statusul %d\n", count, pidWaitNepot, status);
                        if(strcmp(verdict, "SAFE") == 0){//Verificarea continutului din verdict si decid ce fac cu fisierul
                            printf("Nu e corupt\n");
                        } else {
                            printf("Fisierul este corupt si urmeaza sa fie plasat in alt director\n");
                            Coruptie(cale, caleCorupt);
                            continue;
                        }
                    } else {
                        perror("Eroare coruptie\n");
                        exit(-1);
                    }


                }
                continue;//
            }
        }

        if(write(fileDescriptor, curr->d_name, 256) == -1){
            perror("Eroare scriere nume\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_mode, sizeof(st.st_mode)) == -1){
            perror("Eroare scriere mod\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_size, sizeof(st.st_size)) == -1){
            perror("Eroare scriere dimensiune\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_atime, sizeof(st.st_atime)) == -1){
            perror("Eroare scriere ultima accesare\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_ctime, sizeof(st.st_ctime)) == -1){
            perror("Eroare scriere ultima schimbare drepturi\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_mtime, sizeof(st.st_mtime)) == -1){
            perror("Eroare scriere ultima schimbare continut\n");
            exit(-1);
        }

        if(S_ISDIR(st.st_mode)){
            Parse(cale, bin, fileDescriptor, Script, caleCorupt);
        }
        }



    } else {

        while((curr = readdir(dirRef)) != NULL){
        if((strcmp(curr->d_name, ".") == 0) || (strcmp(curr->d_name, "..") == 0)){
            continue;
        }


        printf("%s\n", curr->d_name);
        sprintf(cale, "%s/%s", CD, curr->d_name);

        if(lstat(cale, &st) == -1){
            perror("Eroare statistici 2\n");
            exit(-1);
        }

        if(S_ISREG(st.st_mode)){
            if(!Drepturi(st)){
                count++;
                pid_t pidNepot;
                pid_t pidWaitNepot;
                int status;
                pid_t pfd[2];

                if(pipe(pfd) < 0){
                    perror("Eroare pipe file descriptors\n");
                    exit(-1);
                }

                if((pidNepot = fork()) < 0){
                    perror("Eroare\n");
                    exit(-1);
                }

                if(pidNepot == 0){
                    if(close(pfd[0]) != 0){
                        perror("err pfd\n");
                        exit(-1);
                    }

                    if(dup2(pfd[1], 1) == -1){
                        perror("err pfd\n");
                        exit(-1);
                    }

                    execl("/bin/bash", "bin", Script, cale, NULL);
                    perror("Eroare naspa\n");
                    exit(-1);

                }  else {

                    if(close(pfd[1]) != 0){
                        perror("err pfd\n");
                        exit(-1);
                    }

                    char verdict[256];
                    verdict[strlen(verdict) - 1] = '\0';

                    if(read(pfd[0], verdict, 256) == -1){
                        perror("err pfd\n");
                        exit(-1);
                    }

                    if(close(pfd[0]) != 0){
                        perror("err pfd\n");
                        exit(-1);
                    }

                    pidWaitNepot = wait(&status);
                    if(WIFEXITED(status)){
                        printf("Nepot proces %d cu pid-ul %d s a terminat cu statusul %d\n", count, pidWaitNepot, status);
                        if(strcmp(verdict, "SAFE") == 0){
                            printf("Nu e corupt\n");
                        } else {
                            printf("Fisierul este corupt si urmeaza sa fie plasat in alt director\n");
                            Coruptie(cale, caleCorupt);
                            continue;
                        }
                    } else {
                        perror("Eroare coruptie\n");
                        exit(-1);
                    }


                }
            }
        }

        //citirea secventiala a  datelor din snapshot pt a putea fi comparate cu noile valori
        if(read(fileDescriptor, &nume, 256) == -1){
            perror("Eroare citire nume\n");
            exit(-1);
        }

        lseek(fileDescriptor, -256, SEEK_CUR);

        if(strcmp(nume, curr->d_name) != 0){
            ok = 1;
        }

        //Suprascrierea snapshot ului
        if(write(fileDescriptor, curr->d_name, 256) == -1){
            perror("Eroare scriere nume\n");
            exit(-1);
        }

      ////

        if(read(fileDescriptor, &aux.st_mode, sizeof(aux.st_mode)) == -1){
            perror("Eroare citire mod\n");
            exit(-1);
        }

        if(aux.st_mode != st.st_mode){
            ok = 1;
        }

        lseek(fileDescriptor, -sizeof(aux.st_mode), SEEK_CUR);

        if(write(fileDescriptor, &st.st_mode, sizeof(st.st_mode)) == -1){
            perror("Eroare scriere mod\n");
            exit(-1);
        }
        ////

        if(read(fileDescriptor, &aux.st_size, sizeof(aux.st_size)) == -1){
            perror("Eroare citire dimensiune\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_size), SEEK_CUR);

        if(aux.st_size != st.st_size){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_size, sizeof(st.st_size)) == -1){
            perror("Eroare scriere dimensiune\n");
            exit(-1);
        }


        ////
        if(read(fileDescriptor, &aux.st_atime, sizeof(aux.st_atime)) == -1){
            perror("Eroare citire ultima accesare\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_atime), SEEK_CUR);

        if(aux.st_atime != st.st_atime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_atime, sizeof(st.st_atime)) == -1){
            perror("Eroare scriere ultima accesare\n");
            exit(-1);
        }
        ////
        if(read(fileDescriptor, &aux.st_ctime, sizeof(aux.st_ctime)) == -1){
            perror("Eroare citire ultima schimbare drepturi\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_ctime), SEEK_CUR);

        if(aux.st_ctime != st.st_ctime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_ctime, sizeof(st.st_ctime)) == -1){
            perror("Eroare scriere ultima schimbare drepturi\n");
            exit(-1);
        }
        ////
        if(read(fileDescriptor, &aux.st_mtime, sizeof(aux.st_mtime)) == -1){
            perror("Eroare citire ultima schimbare continut\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_mtime), SEEK_CUR);

        if(aux.st_mtime != st.st_mtime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_mtime, sizeof(st.st_mtime)) == -1){
            perror("Eroare scriere ultima schimbare continut\n");
            exit(-1);
        }

        //Apel recursiv daca intrarea curenta este director
        if(S_ISDIR(st.st_mode)){
            Parse(cale, bin, fileDescriptor, Script, caleCorupt);
        }


    }
        if(ok == 1){//Verificarea statusului directorului
            printf("S a modificat %s \n", CD);
        } else {
            printf("Nu s a modificat %s \n", CD);
        }

    }


    if(closedir(dirRef) != 0){//Inchidere director
        perror("Err inchidere director\n");
        exit(-1);
    }


}





int main(int argc, char **argv){
    if(argc < 8 || argc >16){
        perror("Argumente insuficiente\n");
        exit(-1);
    }

    if(chdir(argv[2]) == -1){
        perror("Nu s a putut schimba directorul\n");
        exit(-1);
    }


    if(strcmp(argv[1], "-o") != 0){
        perror("Argument invalid\n");
        exit(-1);
    }

    if(strcmp(argv[3], "-s") != 0){
        perror("Argument invalid\n");
        exit(-1);
    }

    if(strcmp(argv[5], "-c") != 0){
        perror("Argument invalid\n");
        exit(-1);
    }

    if(strstr(argv[6], ".sh") == NULL){
        perror("Argument invalid shell\n");
        exit(-1);
    }

    pid_t pidProcess;
    pid_t pidWait;
    int status;
    int i;

    //Inceperea executiei in paralel a proceselor
    for(i = 7 ; i<argc; i++){


        if((pidProcess = fork()) < 0){
            perror("Eroare frok\n");
            exit(-1);
        }

        if(pidProcess == 0){//Codul copil
            char statName[256];
            sprintf(statName, "statData-%d.bin", i-6);
            printf("%s\n", statName);

            int bin;
            int fileDescriptor;
            //verificare existenta ceva in director
            fileDescriptor = open(statName, O_RDWR);
            if(fileDescriptor == -1){
                bin = 0;
                printf("Nu exista\n");
                fileDescriptor = open(statName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);

            } else {
                bin = 1;
                printf("Exista\n");
                fileDescriptor = open(statName, O_RDWR);

            }

            //Fiecvare proces va apela functia parse pt directorul sau
            Parse(argv[i], bin, fileDescriptor, argv[6], argv[4]);

            if(close(fileDescriptor) == -1){
                perror("Eroare inchidere\n");
                exit(-1);
            }

            exit(0);
        }

    }


    //Asteptarea finalizarii proceselor copil
    for(i=7; i<argc; i++){
        pidWait = wait(&status);
        printf("%d\n", pidWait);
        if(WIFEXITED(status)){
            printf("Copilul proces %d cu pid-ul %d s a terminat cu statusul %d\n", i-6, pidWait, status);
        } else {
            printf("EROARE\n");
        }
    }



    char cwd[PATH_MAX];//Afisez si verific directorul final in care ma aflu(Snapshots)
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        printf("Directorul curent : %s\n", cwd);
    } else {
        perror("Director invalid\n");
        exit(-1);
    }


    return 0;
}
