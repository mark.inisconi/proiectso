#!/bin/bash


chmod +r "$1"
result=0

pattern1="corrupted"
pattern2="dangerous"
pattern3="risk"
pattern4="attack"
pattern5="malware"
pattern6="malicious"


if grep -q "$pattern1" "$1"; then
result=1
fi

if grep -q "$pattern2" "$1"; then
    result=1
fi

if grep -q "$pattern3" "$1"; then
    result=1
fi

if grep -q "$pattern4" "$1"; then
    result=1
fi

if grep -q "$pattern5" "$1"; then
    result=1
fi

if grep -q "$pattern6" "$1"; then
    result=1
fi

# if [ "$temp_perm" = true ]; then
# chmod -r "$1"
# fi

i=0
linii=0
cuvinte=0
caractere=0

for entity in $(wc "$1") 
do
    if test "$i" -eq 0
    then
        linii=$entity
        i=1
    elif test "$i" -eq 1
    then 
        cuvinte=$entity
        i=2
    elif test "$i" -eq 2
    then 
        caractere=$entity
        i=3
    fi
done

if test "$linii" -le 1 -o "$linii" -ge 10
then 
    result=1
fi

if test "$cuvinte" -le 1 -o "$cuvinte" -ge 20
then 
    result=1
fi

if test "$caractere" -le 1 -o "$caractere" -ge 50
then 
    result=1
fi


if test "$linii" -lt 3 
then
    result=1
fi

if test "$cuvinte" -gt 1000
then 
    result=1
fi

if test "$caractere" -gt 2000
then 
    result=1
fi


chmod -r "$1"
exit $result
