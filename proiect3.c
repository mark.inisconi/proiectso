#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

#define PATH_MAX 4096



void Parse(const char* CD, const char* stFile, int bin, int fileDescriptor){
    int count = 0;
    DIR* dirRef = NULL;
    printf("%s\n", CD);
    if((dirRef = opendir(CD)) == NULL) {
        perror("Nu s a putut deschide directorul curent\n");
        exit(-1);
    }

    


    struct dirent* curr = NULL;//intrarea curenta din director
    struct stat st;//info curente din intrarea curenta


    struct stat aux;
    char nume[256];//d_name e tot de 256
    int ok = 0;

    //printf("%s\n", CD);

    char cale[257];

    if(bin == 0){
        while((curr = readdir(dirRef)) != NULL){

        if((strcmp(curr->d_name, ".") == 0) || (strcmp(curr->d_name, "..") == 0) || (strcmp(curr->d_name, stFile) == 0)){
            continue;
        }

        count++;
        sprintf(cale, "%s/%s", CD, curr->d_name);
        if(lstat(cale, &st) == -1){
            perror("Eroare statistici 1\n");
            exit(-1);
        }

        if(write(fileDescriptor, curr->d_name, 256) == -1){
            perror("Eroare scriere nume\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_mode, sizeof(st.st_mode)) == -1){
            perror("Eroare scriere mod\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_size, sizeof(st.st_size)) == -1){
            perror("Eroare scriere dimensiune\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_atime, sizeof(st.st_atime)) == -1){
            perror("Eroare scriere ultima accesare\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_ctime, sizeof(st.st_ctime)) == -1){
            perror("Eroare scriere ultima schimbare drepturi\n");
            exit(-1);
        }

        if(write(fileDescriptor, &st.st_mtime, sizeof(st.st_mtime)) == -1){
            perror("Eroare scriere ultima schimbare continut\n");
            exit(-1);
        }
        
        if(S_ISDIR(st.st_mode)){
            Parse(cale, stFile, bin, fileDescriptor); 
        }
        }
        printf("%d \n", count);

    } else {
        
        while((curr = readdir(dirRef)) != NULL){
        if((strcmp(curr->d_name, ".") == 0) || (strcmp(curr->d_name, "..") == 0) || (strcmp(curr->d_name, stFile) == 0)){
            continue;
        }

        count++;

        printf("%s\n", curr->d_name);
        sprintf(cale, "%s/%s", CD, curr->d_name);

        if(lstat(cale, &st) == -1){
            perror("Eroare statistici 2\n");
            exit(-1);
        }

        if(read(fileDescriptor, &nume, 256) == -1){
            perror("Eroare citire nume\n");
            exit(-1);
        }

        lseek(fileDescriptor, -256, SEEK_CUR);

        if(strcmp(nume, curr->d_name) != 0){
            ok = 1;
        }

        if(write(fileDescriptor, curr->d_name, 256) == -1){
            perror("Eroare scriere nume\n");
            exit(-1);
        }

      ////

        if(read(fileDescriptor, &aux.st_mode, sizeof(aux.st_mode)) == -1){
            perror("Eroare citire mod\n");
            exit(-1);
        }

        if(aux.st_mode != st.st_mode){
            ok = 1;
        }

        lseek(fileDescriptor, -sizeof(aux.st_mode), SEEK_CUR);

        if(write(fileDescriptor, &st.st_mode, sizeof(st.st_mode)) == -1){
            perror("Eroare scriere mod\n");
            exit(-1);
        }
        ////

        if(read(fileDescriptor, &aux.st_size, sizeof(aux.st_size)) == -1){
            perror("Eroare citire dimensiune\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_size), SEEK_CUR);

        if(aux.st_size != st.st_size){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_size, sizeof(st.st_size)) == -1){
            perror("Eroare scriere dimensiune\n");
            exit(-1);
        }


        ////
        if(read(fileDescriptor, &aux.st_atime, sizeof(aux.st_atime)) == -1){
            perror("Eroare citire ultima accesare\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_atime), SEEK_CUR);

        if(aux.st_atime != st.st_atime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_atime, sizeof(st.st_atime)) == -1){
            perror("Eroare scriere ultima accesare\n");
            exit(-1);
        }
        ////
        if(read(fileDescriptor, &aux.st_ctime, sizeof(aux.st_ctime)) == -1){
            perror("Eroare citire ultima schimbare drepturi\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_ctime), SEEK_CUR);

        if(aux.st_ctime != st.st_ctime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_ctime, sizeof(st.st_ctime)) == -1){
            perror("Eroare scriere ultima schimbare drepturi\n");
            exit(-1);
        }
        ////
        if(read(fileDescriptor, &aux.st_mtime, sizeof(aux.st_mtime)) == -1){
            perror("Eroare citire ultima schimbare continut\n");
            exit(-1);
        }

        lseek(fileDescriptor, -sizeof(aux.st_mtime), SEEK_CUR);

        if(aux.st_mtime != st.st_mtime){
            ok = 1;
        }

        if(write(fileDescriptor, &st.st_mtime, sizeof(st.st_mtime)) == -1){
            perror("Eroare scriere ultima schimbare continut\n");
            exit(-1);
        }

        
        if(S_ISDIR(st.st_mode)){
            Parse(cale, stFile, bin, fileDescriptor); 
        }

        
    }
        if(ok == 1){
            printf("S a modificat %s \n", CD);
        } else {
            printf("Nu s a modificat %s \n", CD);
        }

    }

    printf("%d \n", count);

    
    

}


int main(int argc, char **argv){
    if(argc < 4 && argc >10){
        perror("Argumente insuficiente\n");
        exit(-1);
    }

    if(chdir(argv[2]) == -1){
        perror("Nu s a putut schimba directorul\n");
        exit(-1);
    }


    if(strcmp(argv[1], "-o") != 0){
        perror("Argument invalid\n");
        exit(-1);
    }

    // if(strstr(argv[2], ".bin") == NULL){
    //     perror("Argument invalid stats\n");
    //     exit(-1);
    // }
    for(int i =3 ; i<argc; i++){
        pid_t pidProcess;
        pid_t pidWait;
        int status;

        if((pidProcess = fork()) < 0){
            perror("Eroare frok\n");
            exit(-1);
        }

        if(pidProcess == 0){
            char statName[256];
            sprintf(statName, "statData-%d.bin", i-2);
            printf("%s\n", statName);

            int bin;
            //verificare existenta ceva in director
            int fileDescriptor = open(statName, O_RDWR);
            if(fileDescriptor == -1){
                bin = 0;
                printf("Nu exista\n");
                fileDescriptor = open(statName, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IXUSR);
        
            } else {
                bin = 1;
                printf("Exista\n");
                fileDescriptor = open(statName, O_CREAT| O_RDWR, S_IRUSR | S_IWUSR | S_IXUSR);
        
            }

            Parse(argv[i], argv[2], bin, fileDescriptor);

            if(close(fileDescriptor) == -1){
                perror("Eroare inchidere\n");
                exit(-1);
            }

            exit(0);

            
        } else {
            pidWait = wait(&status);
            if(WIFEXITED(status)){
                printf("Copilul proces %d cu pid-ul %d s a terminat cu statusul %d\n", i-2, pidWait, status);
            }
            else {
            printf("EROARE\n");
            }

        }
    }


    char cwd[PATH_MAX];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        printf("Directorul curent : %s\n", cwd);
    } else {
        perror("Director invalid\n");
        exit(-1);
    }
    

    return 0;
}
